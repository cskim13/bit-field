#ifndef BITFIELD_H
#define BITFIELD H

#include <iostream> 
#include <stdlib.h>
typedef unsigned int TELEM;

class TBitField {
private:
	int BitLen;
	TELEM *pMem;
	int MemLen;

	int GetMemIndex(const int n) const {
		return n >> 4;
	}

	TELEM GetMemMask(const int n) const {
		return 1 << (n & 15);
	}
public:
	TBitField(int len) : BitLen(len) {
		MemLen = (len + 15) >> 4;
		pMem = new TELEM[MemLen];
		if (pMem != NULL) {
			for (int i = 0; i < MemLen; i++) pMem[i] = 0;
		}
	}

	int GetLength(void) const {
		return BitLen;
	}

	void SetBit(const int n) {
		if ((n > -1) && (n < BitLen)) {
			pMem[GetMemIndex(n)] |= GetMemMask(n);
		}
	}

	void ClrBit(const int n) {
		if ((n > -1) && (n < BitLen)) {
			pMem[GetMemIndex(n)] &= GetMemMask(n);
		}
	}

	int GetBit(const int n) const {
		if ((n > -1) && (n < BitLen)) {
			return pMem[GetMemIndex(n)] & GetMemMask(n);
		}
		return 0;
	}

	
};


#endif
