#include "Bitfield.h"
#include <iostream>
using namespace std;

int main()
{
	TBitField myBitField(5);
	cout << "Memory allocation for 5 bits" << endl;
	

	cout << "Set bit 1" << endl;
	myBitField.SetBit(0);
	cout << "Set bit 2" << endl;
	myBitField.SetBit(1);
	cout << "Set bit 3" << endl;
	myBitField.SetBit(2);
	cout << "Set bit 4" << endl;
	myBitField.SetBit(3);
	cout << "Set bit 5" << endl;
	myBitField.SetBit(4);

	cout << "Print bit field:\n" << endl;
	for (int i = 0; i < myBitField.GetLength(); i++) {
		cout << myBitField.GetBit(i) << " ";
	}
	cout <<"\n"<< endl;

	cout << "Delete bit 1" << endl;
	myBitField.ClrBit(0);
	cout << "Delete bit 2" << endl;
	myBitField.ClrBit(1);
	cout << "Delete bit 3" << endl;
	myBitField.ClrBit(2);
	cout << "Delete bit 4" << endl;
	myBitField.ClrBit(3);
	cout << "Delete bit 5" << endl;
	myBitField.ClrBit(4);

	cout << "Print bit field:\n" << endl;
	for (int i = 0; i < myBitField.GetLength(); i++) {
		cout << myBitField.GetBit(i) << " ";
	}
	getchar();
	return 0;
}